import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    titles = []
    for i, value in enumerate(df['Name']):
        name = value.split(', ')[1]
        title = name.split(' ')[0]
        titles.append(title)

    df['Title'] = titles # df['Name'].str.extract(' ([A-Za-z]+\.)')
    titles_to_consider = ["Mr.", "Mrs.", "Miss."]
    results = []

    for title in titles_to_consider:
        median_age = df[df['Title'] == title]['Age'].median()
        missing_values = df[(df['Title'] == title) & df['Age'].isnull()].shape[0]
        results.append((title, missing_values, round(median_age)))

    return results
